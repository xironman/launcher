/*******************************************************************************
 * launcher
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <ctype.h>

#include "launcher.h"

/*
 *========================================================================
 * Name:   trimReverse
 * Prototype:  char *trimReverse( char * )
 *
 * Description:
 * Trim whitespace from the end of a buffer by replacing spaces with \0.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 *========================================================================
 */
void
trimReverse( char *ptr )
{
    // Trim leading space
    while(isspace(*ptr)) 
	{
		*ptr = '\0';
        ptr--;
	}
}
