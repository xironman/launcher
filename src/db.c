/*******************************************************************************
 * launcher
 *
 * db.c:  Functions for reading and managing a app description files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <glib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "launcher.h"

GSList  *appList = NULL;
APP_T *activeApp = NULL;

/*
 *========================================================================
 * Name:   appSorter
 * Prototype:  gint appSorter( gconstpointer, gconstpointer )
 *
 * Description:
 * A Comparison function for inserting apps by name alphabetically.
 * 
 * Notes:
 * First argument is an existing list element.  Second argument is the app
 * to be added to the list.
 *========================================================================
 */
gint 
appSorter (gconstpointer app1, gconstpointer app2)
{
    APP_T *existingApp = (APP_T *)app1;
    APP_T *newApp = (APP_T *)app2;
    if ( app1 == NULL )
        return 1;
    if ( app2 == NULL )
        return 0;
    return ((gint) strcasecmp ((char *)existingApp->appName, (char *)newApp->appName));
}

/*
 *========================================================================
 * Name:   freeAppEntry
 * Prototype:  void freeAppEntry( APP_T *ptr )
 *
 * Description:
 * Free up alllocated storage for an app entry.
 *========================================================================
 */
void 
freeAppEntry (APP_T *ptr)
{
    if ( ptr == NULL )
        return;
    if ( ptr->appName != NULL ) g_free(ptr->appName);
    if ( ptr->author != NULL ) g_free(ptr->author);
    if ( ptr->contact != NULL ) g_free(ptr->contact);
    if ( ptr->command != NULL ) g_free(ptr->command);
    if ( ptr->icon != NULL ) g_free(ptr->icon);
    if ( ptr->splash != NULL ) g_free(ptr->splash);
    if ( ptr->description != NULL ) g_free(ptr->description);
}

/*
 *========================================================================
 * Name:   parseElement
 * Prototype:  void parseElement( xmlDocPtr doc, xmlNodePtr *ptr )
 *
 * Description:
 * Parse the property from an XML element.
 *========================================================================
 */
char *
parseElement ( xmlDocPtr doc, xmlNodePtr cur, char *propName )
{
    xmlChar *key;
    char *val = NULL;
    char *ptr1, *ptr2;

    key = xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
    if ( key != NULL )
    {
        val = g_strdup( (char *)key );
        ptr1 = piboxTrim( val );
        trimReverse( (char *)(val + strlen(val)-1));
        ptr2 = g_strdup( ptr1 );
        g_free(val);
        val = ptr2;
        piboxLogger(LOG_INFO, "Key = *%s*\n", val);
    }
    xmlFree ( key );
    return val;
}

/*
 *========================================================================
 * Name:   dbLoad
 * Prototype:  void dbLoad( void )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Returns:
 * -1 on failure, 0 otherwise.
 *
 * Notes:
 * We changed to the top of the db directory tree when we started, so 
 * we just search for db's from the current location.
 *========================================================================
 */
int 
dbLoad()
{
    APP_T           *app = NULL;
    xmlDocPtr       doc;
    xmlNodePtr      cur_node;
    char            *msg = NULL;

    char            *appDir = NULL;
    DIR             *dir = NULL;
    struct dirent   *entry = NULL;
    int             cnt = 0;

    // Get the current directory, which should be the directory
    // where the application description files are.
    appDir = getcwd(NULL, 0);

    // Find all configuration files in app directory.
    dir = opendir(appDir);
    if ( dir == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory: %s\n", appDir);
        return -1;
    }

    // For each file in the directory, parse as XML.
    while( (entry=readdir(dir)) != NULL )
    {
        if ( entry->d_name[0] == '.' )
            continue;

        // We were designed to run on Linux, so this should work.
        if ( entry->d_type == DT_DIR )
            continue;
		piboxLogger(LOG_INFO, "App Description file: %s\n", entry->d_name);

        // Read file and parse as xml 
        doc = xmlReadFile(entry->d_name, NULL, XML_PARSE_NOERROR);
        if ( doc == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to parse %s\n", entry->d_name);
            continue;
        }
        cur_node=xmlDocGetRootElement(doc);
        if ( cur_node == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to get root node.\n", entry->d_name);
            continue;
        }

        // Create an APP_T instance
        app = g_new0(APP_T, 1);

        // Run the document tree and extract fields of interest into
        // the APP_T instance.  This is very simple: one element per config item.
        for(cur_node=cur_node->xmlChildrenNode; cur_node; cur_node=cur_node->next)
        {
            if (cur_node->type != XML_ELEMENT_NODE)
                continue;

            piboxLogger(LOG_INFO, "Node name: %s\n", cur_node->name);
            if ( xmlStrcasecmp(cur_node->name, (xmlChar *)"launcher") == 0 )
                continue;

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "appName") == 0 )
                app->appName = parseElement(doc, cur_node, "appName");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Author") == 0 )
                app->author = parseElement(doc, cur_node, "Author");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Contact") == 0 )
                app->contact = parseElement(doc, cur_node, "Contact");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Command") == 0 )
                app->command = parseElement(doc, cur_node, "Command");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Icon") == 0 )
                app->icon = parseElement(doc, cur_node, "Icon");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Splash") == 0 )
                app->splash = parseElement(doc, cur_node, "Splash");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Description") == 0 )
                app->description = parseElement(doc, cur_node, "Description");
        }

        // Validate the entry is complete
        if ( app->appName == NULL ) msg = "Missing appName.";
        else if ( app->author == NULL ) msg = "Missing author.";
        else if ( app->contact == NULL ) msg = "Missing contact.";
        else if ( app->command == NULL ) msg = "Missing command.";
        else if ( app->icon == NULL ) msg = "Missing icon.";
        else if ( app->splash == NULL ) msg = "Missing splash.";
        else if ( app->description == NULL ) msg = "Missing description.";

        // Clean up description
        piboxStripNewline( app->description );

        if ( msg != NULL )
        {
            piboxLogger(LOG_ERROR, "Incomplete description file: %s\n", entry->d_name);
            piboxLogger(LOG_ERROR, "%s\n", msg);
            freeAppEntry(app);
            g_free(app);
            continue;
        }
        
        // Add it to our link list.
        piboxLogger(LOG_INFO, "Loaded app: %s\n", app->appName);
        appList = g_slist_insert_sorted(appList, app, appSorter);

        cnt++;
    }
    closedir(dir);

    piboxLogger(LOG_INFO, "Application descriptions found: %d\n", cnt);

    return cnt;
}

/*
 *========================================================================
 * Name:   dbGetEntry
 * Prototype:  void dbGetEntry( gint idx )
 *
 * Description:
 * Retrieve the app entry at the specified index.
 *========================================================================
 */
APP_T *
dbGetEntry (gint idx)
{
    return g_slist_nth_data(appList, idx);
}

/*
 *========================================================================
 * Name:   dbGetLength
 * Prototype:  guint dbGetLength( void )
 *
 * Description:
 * Retrieve the app entry at the specified index.
 *========================================================================
 */
guint
dbGetLength ( void )
{
    return g_slist_length(appList);
}

