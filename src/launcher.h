/*******************************************************************************
 * launcher
 *
 * launcher.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <gtk/gtk.h>

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef LAUNCHER_C
int     daemonEnabled = 0;
char    errBuf[256];
gchar   *imagePath = NULL;
#else
extern int      daemonEnabled;
extern char     errBuf[];
extern gchar   *imagePath;
#endif /* LAUNCHER_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "launcher"
#define MAXBUF      4096
#define APPMGR_PORT 13912
#define SPLASH_W    450
#define SPLASH_H    450

#define MASK_T      "Mask.png"
#define MASK        "/etc/launcher/Mask.png"
#define UNMASK_T    "Unmask.png"
#define UNMASK      "/etc/launcher/Unmask.png"

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/utils.h>
#include "db.h"
#include "cli.h"
#include "utils.h"

#endif /* !LAUNCHER_H */

