/*******************************************************************************
 * launcher
 *
 * db.h:  Functions for reading a VideoLib database.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_H

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/*
 * A single app record
 */
typedef struct _app_t {
	char	*next;
	char	*prev;
	char	*appName;
	char	*author;
	char	*contact;
	char	*command;
	char	*icon;
	char	*splash;
	char	*description;
} APP_T;

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

// Production location for application description files.
#define DBTOP		"/etc/launcher"

// Test location for local media
#define DBTOP_T		"./data"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern int      dbLoad( void );
extern void     populateList (GtkListStore *listStore);
extern void     updatePoster (GtkTreeSelection *, gpointer);
extern APP_T    *dbGetEntry (gint idx);
extern guint    dbGetLength ( void );
#endif
