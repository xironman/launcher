/*******************************************************************************
 * launcher
 *
 * launcher.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define LAUNCHER_C

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <cairo.h>
#include <math.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>

#include "launcher.h" 

/* Local prototypes */
static gboolean draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
gboolean iconTouchGTK( gpointer data );
void selectApp( int idx );
void notify(void);

int maxApps = 0;
GdkPixbuf *image = NULL;
GdkPixmap *clockPixmap = NULL;
cairo_t *cr = NULL;
cairo_t *clock_cr = NULL;

GtkWidget *darea[12];
GtkWidget *iconTable = NULL;
GtkWidget *appSplash = NULL;
GtkTextBuffer *appDesc = NULL;
guint cellWidth = 0;
guint cellHeight = 0;
guint iconPad = 18;

int currentIdx = 0;

int enableTouch = 1;

/*
 *========================================================================
 * Name:   iconTouch
 * Prototype:  void iconTouch( int region )
 *
 * Description:
 * Handler for touch location reports. 
 *========================================================================
 */
void
iconTouch( REGION_T *region )
{
    g_idle_add( (GSourceFunc)iconTouchGTK, (gpointer)region );
}

gboolean
iconTouchGTK( gpointer data )
{
    static int          inprogress = 0;
    int                 idx = 0;
    gint                wx, wy;
    REGION_T            *region = (REGION_T *)data;
    GtkRequisition      req;

    piboxLogger(LOG_INFO, "Entered.\n");

    if ( !enableTouch )
    {
        piboxLogger(LOG_INFO, "Touch processing disabled, skipping update\n");
        return FALSE;
    }

    /* Prevent multiple touches from being handled at the same time. */
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return FALSE;
    }
    inprogress = 1;
    piboxLogger(LOG_INFO, "Processing touch request.\n");

    /* Find the widget that maps to the absolute coordinates */
    while( darea[idx] != NULL )
    {
        /* Get the absolute screen coordinates of the widget */
        gdk_window_get_origin (gtk_widget_get_window (darea[idx]), &wx, &wy);
        req.width = gdk_window_get_width(darea[idx]->window);
        req.height = gdk_window_get_height(darea[idx]->window);
        piboxLogger(LOG_INFO, "origin, wxh: %d %d  %d / %d\n", wx, wy, req.width, req.height);

        /* Test if the event was within the bounds of the widget area. */
        if ( (region->x >= wx) && (region->x<= (wx+req.width)) )
        {
            if ( (region->y >= wy) && (region->y<= (wy+req.height)) )
            {
                /* Found widget! */
                piboxLogger(LOG_INFO, "Identified table cell: %d\n", idx);
                currentIdx = idx;
                selectApp(currentIdx);
                notify();
                inprogress = 0;
                enableTouch = 0;
                return(FALSE);
            }
        }
        idx++;
    }
    inprogress = 0;
    piboxLogger(LOG_INFO, "No matching table cell identified for %d / %d\n", region->x, region->y);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();

    /* Default settings if we don't have any others. */
    if ( (width==0) || (height==0) )
    {
        width=800;
        height=480;
    }

    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
        iconPad = 9;
    }

    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        setCLIFlag(CLI_TOUCH);
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   updateClock
 * Prototype:  gboolean updateClock( gpointer data )
 *
 * Description:
 * Update the clock area with the current hour/minute.
 * This is just an example.  Should use double buffering here (draw in pixmap first).
 *========================================================================
 */
gboolean updateClock (gpointer data)
{
    gdouble                 x, y, m, n, radius;
    cairo_text_extents_t    extents;
    time_t                  now;
    struct tm*              timeinfo;
    int                     width, height;
    int                     hour;
    int                     minute;
    int                     rc;
    char                    buf[9];
    char                    *ptr;

    time(&now);
    timeinfo = localtime(&now);
    hour = timeinfo->tm_hour;
    minute = timeinfo->tm_min;
    ptr = (hour<12)?"AM":"PM";

    /* Adjust hour to AM/PM */
    hour = ((hour<12)?hour+1:((hour==12)?hour:hour-12))%12;

    memset(buf, 0, 9);
    rc = snprintf(buf, 9, "%2d:%02d %.2s", hour%12, minute%60, ptr);

    /* Test RC: This avoids -WFormat-Truncation warnings when used with GCC 7.1 or later */
    if (rc < 0) {
        return TRUE;
    }
    piboxLogger(LOG_INFO, "buf: %s\n", buf);

    x = appSplash->allocation.width / 2;
    y = appSplash->allocation.height / 2;
    radius = MIN ((appSplash->allocation.width*0.9) / 2, (appSplash->allocation.height*0.9) / 2);
    piboxLogger(LOG_INFO, "window x/y, radius: %f / %f / %f\n", x, y, radius);

    /* If we don't have a pixmap to draw in, get one. */
    if ( clockPixmap == NULL )
    {
        clockPixmap = gdk_pixmap_new(appSplash->window,appSplash->allocation.width,appSplash->allocation.height,-1);
    }

    /* Setup the pixmap as the thing we draw into with Cairo */
    gdk_drawable_get_size(clockPixmap, &width, &height);
    piboxLogger(LOG_INFO, "pixmap w/h: %d/%d\n", width, height);
    cairo_surface_t *cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    clock_cr = cairo_create(cst);

    // clock_cr = gdk_cairo_create(appSplash->window);

    /* Reset to background color */
    cairo_set_source_rgb(clock_cr, 0.13, 0.13, 0.13);
    cairo_rectangle(clock_cr, 0, 0, width, height);
    cairo_fill(clock_cr);

    /* Draw a circular background */
    cairo_arc(clock_cr, x, y, radius+1, 0, 2*M_PI);
    cairo_set_source_rgb(clock_cr, 0.2, 0.2, 0.2);
    cairo_fill_preserve(clock_cr);
    cairo_stroke(clock_cr);

    /* Overlay the time as text */
    cairo_set_source_rgba(clock_cr, 0.8, 0.4, 0.0, 0.5);
    cairo_select_font_face(clock_cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(clock_cr, 60);
    cairo_text_extents(clock_cr, buf, &extents);
    piboxLogger(LOG_INFO, "extents w/h: %d / %d\n", extents.width, extents.height);

    piboxLogger(LOG_INFO, "pre x/w: %f / %f\n", x, extents.width);
    m = x - (extents.width*0.55); /* No idea but why, but slightly more than half is required! */
    n = y + (extents.height/2.0);;
    piboxLogger(LOG_INFO, "text m/n: %f / %f\n", m, n);

    cairo_move_to(clock_cr, m, n);
    cairo_show_text(clock_cr, buf);

    /* Clean up */
    cairo_destroy(clock_cr);
    clock_cr = NULL;

    cairo_t *cr_pixmap = gdk_cairo_create(clockPixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);

    /*
     * Now copy the pixmap over to the widget.
     */
    gdk_draw_drawable(appSplash->window,
        appSplash->style->fg_gc[GTK_WIDGET_STATE(appSplash)], clockPixmap,
        0, 0,
        0, 0,
        -1, -1);

    return TRUE;
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, char *, int, int )
 *
 * Description:
 * Updates the poster area of the display.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * char *           The path to the image file to fill in the drawing.
 * int              If 1, scale the image to fit the widget
 * int              0: no highlight; 1: draw a highlight beneath the image; 2: clear highlight
 *========================================================================
 */
void 
do_drawing( GtkWidget *drarea, char *path, int doSplash, int doHighlight)
{
    gint            iwidth, iheight;
    gint            width, height;
    gint            swidth, sheight;
    gint            awidth, aheight;
    gdouble         offset_x, offset_y;
    GtkRequisition  req;
    char            maskPath[256];
    GdkPixbuf       *newimage;
    gfloat          ri, rs;

    cr = gdk_cairo_create(drarea->window);
    req.width = gdk_window_get_width(drarea->window);
    req.height = gdk_window_get_height(drarea->window);
    awidth = (int)(req.width * .9);
    aheight = (int)(req.height * .9);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);
    piboxLogger(LOG_INFO, "adjusted window w/h: %d / %d\n", awidth, aheight);

    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(maskPath, "%s", MASK_T);
    else
        sprintf(maskPath, "%s", MASK);

    /*
     * If requested, draw a background highlight
     * The highlight image has been pre-scaled to fit.
     */
    if ( doHighlight == 1 )
    {
        piboxLogger(LOG_INFO, "Adding a highlight: %s\n", maskPath);
        image  = gdk_pixbuf_new_from_file(path, NULL);
        iwidth  = gdk_pixbuf_get_width(image);
        iheight = gdk_pixbuf_get_height(image);
        g_object_unref(image);

        image  = gdk_pixbuf_new_from_file(maskPath, NULL);
        piboxLogger(LOG_INFO, "highlight size: %d / %d\n", iwidth, iheight);

        /* Compute scaling to keep aspect ratio but fit in the screen */
        ri = (gfloat)((gfloat)iwidth / (gfloat)iheight);
        rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
        if ( rs > ri )
        {
            swidth = (gint)(iwidth * aheight/iheight);
            sheight = aheight;
        }
        else
        {
            swidth = awidth;
            sheight = (gint)(iheight * awidth/iwidth);
        }
        swidth  += iconPad;
        sheight += iconPad;
        piboxLogger(LOG_INFO, "updated highlight size: %d / %d\n", swidth, sheight);
        newimage = gdk_pixbuf_scale_simple(image, swidth, sheight, GDK_INTERP_BILINEAR);

        /* Positioning */
        offset_x = (req.width - swidth) / 2.0;
        offset_y = (req.height - sheight) / 2.0;
        gdk_cairo_set_source_pixbuf(cr, newimage, offset_x, offset_y);

        cairo_paint(cr);    
        g_object_unref(image);
        g_object_unref(newimage);
    }

    // Or clear the highlight
    else if ( doHighlight == 2 )
    {
        piboxLogger(LOG_INFO, "Clearing highlight\n");
        image  = gdk_pixbuf_new_from_file(maskPath, NULL);
        width  = gdk_pixbuf_get_width(image);
        height = gdk_pixbuf_get_height(image);

        /* Compute scaling to keep aspect ratio but fit in the screen */
        ri = (gfloat)((gfloat)width / (gfloat)height);
        rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
        if ( rs > ri )
        {
            swidth = (gint)(width * aheight/height);
            sheight = aheight;
        }
        else
        {
            swidth = awidth;
            sheight = (gint)(height * awidth/width);
        }

        offset_x = (req.width - swidth) / 2.0;
        offset_y = (req.height - sheight) / 2.0;
        cairo_set_source_rgba(cr, 0.13, 0.13, 0.13, 1.0);
        cairo_rectangle(cr, offset_x, offset_y, (double)swidth, (double)sheight);
        cairo_fill(cr); 
        cairo_paint(cr);    
        g_object_unref(image);
    }

    /* Load the appIcon or appSplash, depending on who called us. */
    image  = gdk_pixbuf_new_from_file(path, NULL);
    width  = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);

    piboxLogger(LOG_INFO, "Icon image: %s: w/h: %d / %d \n", path, width, height);
    /* Compute scaling to keep aspect ratio but fit in the screen */
    ri = (gfloat)((gfloat)width / (gfloat)height);
    rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
    if ( rs > ri )
    {
        piboxLogger(LOG_INFO, "rs > ri\n");
        swidth = (gint)(width * aheight/height);
        sheight = aheight;
    }
    else
    {
        piboxLogger(LOG_INFO, "rs <= ri\n");
        swidth = awidth;
        sheight = (gint)(height * awidth/width);
    }
    piboxLogger(LOG_INFO, "updated icon size: %d / %d\n", swidth, sheight);
    newimage = gdk_pixbuf_scale_simple(image, swidth, sheight, GDK_INTERP_BILINEAR);

    /* Positioning */
    offset_x = (double)(req.width - swidth) / (double)2.0;
    offset_y = (double)(req.height - sheight) / (double)2.0;
    gdk_cairo_set_source_pixbuf(cr, newimage, offset_x, offset_y);
    g_object_unref(image);
    g_object_unref(newimage);

    cairo_paint(cr);    
    cairo_destroy(cr);
    cr = NULL;
    image = NULL;
}

/*
 *========================================================================
 * Name:   draw_icon
 * Prototype:  void draw_icon( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the poster area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean 
draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{      
    struct stat     stat_buf;
    GtkWidget       *drarea;
    APP_T           *appEntry;
    gint            idx = GPOINTER_TO_INT(user_data);

    // Get the app entry for this index
    appEntry = dbGetEntry(idx);
    if (appEntry == NULL )
        return FALSE;

    if ( appEntry->icon == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing icon for app %s\n", appEntry->appName);
        return FALSE;
    }
    if ( stat(appEntry->icon, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "icon does not exist: %s\n", appEntry->icon);
        return FALSE;
    }

    /* Get drawing area widget */
    drarea = darea[idx];

    do_drawing(drarea, appEntry->icon, FALSE, 0);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   selectApp
 * Prototype:  void selectApp( gint idx )
 *
 * Description:
 * Fills in the description for the selected app.
 *========================================================================
 */
void 
selectApp( int idx )
{
    APP_T           *appEntry;

    piboxLogger(LOG_INFO, "Entered selectApp\n");

    /* Highlight the app icon */
    appEntry = dbGetEntry(currentIdx);
    if (appEntry != NULL )
    {
        piboxLogger(LOG_INFO, "Calling doDrawing %d\n", currentIdx);
        do_drawing(darea[currentIdx], appEntry->icon, FALSE, 2);
    }

    appEntry = dbGetEntry(idx);
    if (appEntry != NULL )
    {
        piboxLogger(LOG_INFO, "Calling doDrawing idx: %d\n", idx);
        do_drawing(darea[idx], appEntry->icon, FALSE, 1);
    }
    currentIdx = idx;

    /* Update the description field */
    if ( appEntry->description != NULL )
        gtk_text_buffer_set_text(appDesc, appEntry->description, -1);
}

/*
 *========================================================================
 * Name:   notify
 * Prototype:  void notify( void )
 *
 * Description:
 * Sends a packet to the appManager to tell it to start an app.
 *========================================================================
 */
void 
notify(void)
{      
    int                 sockfd = 0;
    int                 hdr;
    int                 size;
    int                 n;
    APP_T               *appEntry;
    struct sockaddr_in  serv_addr; 

    // Initialization of buffers
    memset(&serv_addr, '0', sizeof(serv_addr)); 

    // Create a socket for the message
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        piboxLogger(LOG_ERROR, "Could not create socket to appMgr: %s\n", strerror(errno));
        return;
    } 

    // Setup TCP connection to appMgr port
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(APPMGR_PORT); 

    // appMgr must be running on localhost
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        piboxLogger(LOG_ERROR, "inet_pton error: %s\n", strerror(errno));
        return;
    } 

    // Connect to appMgr
    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       piboxLogger(LOG_ERROR, "Connect Failed %s \n", strerror(errno));
       return;
    } 

    // Build a packet
    // 8bit header = 00000001
    hdr = 0x00000001;

    // Get the app entry for this index
    appEntry = dbGetEntry(currentIdx);
    if (appEntry == NULL )
    {
       piboxLogger(LOG_ERROR, "No appEntry for index %d\n", currentIdx);
       return;
    } 

    // command string
    if ( appEntry->command == NULL )
    {
       piboxLogger(LOG_ERROR, "Missing command for app: %s\n", appEntry->appName);
       return;
    } 
    piboxLogger(LOG_INFO, "Command to run: %s\n", appEntry->command);

    // 8bit size = length of command
    size = strlen(appEntry->command);

    // Send it
    n = write(sockfd,&hdr,4);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing hdr: %s", strerror(errno));
         return;
    }
    n = write(sockfd,&size,4);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing size: %s", strerror(errno));
         return;
    }
    n = write(sockfd,appEntry->command,size);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing command: %s", strerror(errno));
         return;
    }

    // Close the socket
    close(sockfd);
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start an application.
 *========================================================================
 */
static gboolean 
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{      
    piboxLogger(LOG_INFO, "Button: x %f, y %f\n", event->x, event->y);
    switch(event->type) {
        case GDK_BUTTON_PRESS:
            if ( event->button == 1 )
            {
                // select active item.
                enableTouch = 0;
                notify();
                piboxLogger(LOG_INFO, "Disabling touch support.\n");
                return(TRUE);
            }
            break;
        default:
            break;
    }
    return(TRUE);
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start an application.
 *========================================================================
 */
static gboolean 
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    int idx = currentIdx;
    piboxLogger(LOG_INFO, "Key: %s\n", gdk_keyval_name(event->keyval));
    switch(event->keyval) 
    {
        case GDK_Left:
        case GDK_KP_Left:
            piboxLogger(LOG_INFO, "Left key\n");
            idx--;
            break;

        case GDK_Right:
        case GDK_KP_Right:
            piboxLogger(LOG_INFO, "Right key\n");
            idx++;
            break;

        case GDK_Up:
        case GDK_KP_Up:
            piboxLogger(LOG_INFO, "Up key\n");
            idx -=3;
            break;

        case GDK_Down:
        case GDK_KP_Down:
            piboxLogger(LOG_INFO, "Down key\n");
            idx +=3;
            break;

        case GDK_Return:
        case GDK_KP_Enter:
            piboxLogger(LOG_INFO, "Return key\n");
            enableTouch = 0;
            notify();
            return(TRUE);
            break;

        default:
            piboxLogger(LOG_INFO, "Key: %s\n", gdk_keyval_name(event->keyval));
            break;
    }

    // Clamp the idx
    if ( idx < 0 )
        idx = 0;
    else if ( idx > maxApps-1 )
        idx = maxApps-1;

    // Update the display
    selectApp(idx);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   initApp
 * Prototype:  void initApp( gint idx )
 *
 * Description:
 * Called when window is exposed (like the first time it is opened) to
 * make sure the first app is highlighted correctly.
 *========================================================================
 */
static gboolean 
initApp(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    selectApp(currentIdx);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with icons on the left and a clock
 * on the right.  
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *window;
    GtkWidget           *hbox1;
    GtkWidget           *vbox0;
    GtkWidget           *dwga;
    GtkWidget           *view;
    gint                row, col, idx;
    guint               appCount;
    guint               numRows;
    guint               numCols;
    guint               splashWidth;
    guint               splashHeight;
    guint               windowWidth;
    guint               windowHeight;

    PangoFontDescription *fd = NULL;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(window), "expose_event", G_CALLBACK(initApp), NULL); 

    if ( piboxGetDisplayType() != PIBOX_LCD )
    {
        g_signal_connect(G_OBJECT(window), 
                "key_press_event", 
                G_CALLBACK(key_press), 
                NULL); 
        g_signal_connect(G_OBJECT(window), 
                "button_press_event", 
                G_CALLBACK(button_press), 
                NULL); 
    }

    /* Two Rows: bottom is alarms, top is for icons and clock. */
    vbox0 = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox0, "vbox0");
    gtk_container_add (GTK_CONTAINER (window), vbox0);

    /* Two columns: app icons and clock */
    hbox1 = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox1, "hbox1");
    gtk_container_add (GTK_CONTAINER (vbox0), hbox1);

    /* 
     * How many rows in the table do we need?
     * We only support 9 apps total for now.
     */
    appCount = dbGetLength();
    piboxLogger(LOG_INFO, "Number of apps: %d\n", appCount);
    numRows = appCount;
    numCols = 1;
    piboxLogger(LOG_INFO, "Number of rows: %d\n", numRows);

    splashWidth  = SPLASH_W;
    if ( (int)(piboxGetDisplayHeight()*.75) < SPLASH_H )
    {
        splashHeight = SPLASH_H;
    }
    else
    {
        splashHeight = 578;
    }
    piboxLogger(LOG_INFO, "Splash w/h: %d / %d\n", splashWidth, splashHeight);

    cellWidth = (piboxGetDisplayWidth() - splashWidth) / numCols;
    cellHeight = (piboxGetDisplayHeight() - splashHeight) / numRows;

    /* First column: table of application icons */
    iconTable = gtk_table_new(numRows,numCols,TRUE);
    gtk_widget_set_size_request(iconTable, piboxGetDisplayWidth() - splashWidth, piboxGetDisplayHeight());
    gtk_box_pack_start (GTK_BOX (hbox1), iconTable, TRUE, TRUE, 0);

    /*
     * A set of Application Icons
     */
    memset(darea, 0, sizeof(darea));
    idx = 0;
    for (row=0; row<numRows; row++)
    {
        for (col=0; col<numCols; col++)
        {
            dwga = gtk_drawing_area_new();
            gtk_widget_set_size_request(dwga, cellWidth, cellHeight);
            g_signal_connect(G_OBJECT(dwga), "expose_event", G_CALLBACK(draw_icon), GINT_TO_POINTER(idx)); 
            gtk_table_attach_defaults(GTK_TABLE(iconTable), dwga, col, col+1, row, row+1 );
            darea[idx] = dwga;

            idx++;
        }
    }

    /*
     * The Application Splash (in this case:  the local time)
     */
    appSplash = gtk_drawing_area_new();
    gtk_box_pack_start (GTK_BOX (hbox1), appSplash, TRUE, TRUE, 0);
    gtk_widget_set_size_request(appSplash, splashWidth, splashHeight);

    /*
     * The Warnings box.
     */
    view = gtk_text_view_new();
    gtk_widget_set_size_request(view, splashWidth, piboxGetDisplayHeight()-splashHeight+20);
    piboxLogger(LOG_INFO, "text view height: %d\n", piboxGetDisplayHeight()-splashHeight+20);
    appDesc = gtk_text_view_get_buffer( GTK_TEXT_VIEW(view) );
    fd = pango_font_description_from_string ("Italic 20");
    gtk_widget_modify_font (view, fd);
    pango_font_description_free (fd);
    gtk_text_view_set_left_margin (GTK_TEXT_VIEW(view), 10);
    gtk_text_view_set_right_margin (GTK_TEXT_VIEW(view), 10);
    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(view), GTK_WRAP_WORD);
    gtk_text_view_set_justification (GTK_TEXT_VIEW(view), GTK_JUSTIFY_CENTER);
    gtk_text_view_set_editable (GTK_TEXT_VIEW(view), FALSE);
    gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW(view), FALSE);
    gtk_text_view_set_pixels_below_lines (GTK_TEXT_VIEW(view), 20);

    gtk_box_pack_start (GTK_BOX (vbox0), view, FALSE, FALSE, 0);

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    windowWidth= piboxGetDisplayWidth();
    if ( windowWidth == 0 )
        windowWidth = 800;
    windowHeight= piboxGetDisplayHeight();
    if ( windowHeight == 0 )
        windowHeight = 480;
        
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), windowWidth, windowHeight);
    gtk_window_set_title(GTK_WINDOW(window), "Launcher");

    return window;
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            // TBD
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            // TBD
            break;

        case SIGUSR1:
            /* Bring it down gracefully. */
            piboxLogger(LOG_INFO, "Re-enabling touch support.\n");
            enableTouch = 1;
            break;
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int 
main(int argc, char *argv[])
{
    GtkWidget           *window;
    struct stat         stat_buf;
    char                path[MAXBUF];
    struct sigaction    sa;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {   
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGUSR1.\n", PROG);
        exit(1);
    }

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    
    // Set the path to the where the application description files live.
    memset(path, 0, MAXBUF);
    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(path, "%s", DBTOP_T);
    else
        sprintf(path, "%s", DBTOP);

    // Test for db existance.
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory");
        return(1);
    }

    // Change to that directory
    chdir(path);

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));

    // Load db
    maxApps = dbLoad();
    if ( maxApps == 0 )
    {
        piboxLogger(LOG_ERROR, "No applications found.\n");
        return -1;
    }

    /* Get display config information */
    loadDisplayConfig();

    /* 
     * If we're on a touchscreen, register the input handler.
     */
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(iconTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }

    // Initialize gtk, create its windows and display them.
    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse("./gtkrc");
    window = createWindow();
    gtk_widget_show_all(window);

    g_timeout_add(1000, updateClock, NULL);
    gtk_main();
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        piboxTouchShutdownProcessor();
    }
    piboxLoggerShutdown();
    return 0;
}
